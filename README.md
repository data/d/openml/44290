# OpenML dataset: Meta_Album_RESISC_Mini

https://www.openml.org/d/44290

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album RESISC Dataset (Mini)**
***
RESISC45 dataset(https://gcheng-nwpu.github.io/) gathers 700 RGB images of size 256x256 px for each of 45 scene categories. The data authors strive to provide a challenging dataset by increasing both within-class diversity and between-class similarity, as well as integrating many image variations. Even though RESISC45 does not propose a label hierarchy, it can be created from other common aerial image label organization scheme. We have preprocessed RESISC for Meta-Album by resizing the dataset to 128x128 px.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/RESISC.png)

**Meta Album ID**: REM_SEN.RESISC  
**Meta Album URL**: [https://meta-album.github.io/datasets/RESISC.html](https://meta-album.github.io/datasets/RESISC.html)  
**Domain ID**: REM_SEN  
**Domain Name**: Remote Sensing  
**Dataset ID**: RESISC  
**Dataset Name**: RESISC  
**Short Description**: Remote sensing dataset  
**\# Classes**: 45  
**\# Images**: 1800  
**Keywords**: remote sensing, satellite image, aerial image, land cover  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: CC-BY-NC 4.0  
**License URL(original data release)**: https://creativecommons.org/licenses/by-nc/4.0/
 
**License (Meta-Album data release)**: CC-BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: NWPU-RESISC45 Dataset  
**Source URL**: https://gcheng-nwpu.github.io/  
  
**Original Author**: Gong Cheng, Junwei Han, and Xiaoqiang Lu  
**Original contact**: chenggong1119@gmail.com  

**Meta Album author**: Phan Anh VU  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@article{DBLP:journals/corr/ChengHL17,
  author    = {Gong Cheng and Junwei Han and Xiaoqiang Lu},
  title     = {Remote Sensing Image Scene Classification: Benchmark and State of the Art},
  journal   = {CoRR},
  volume    = {abs/1703.00121},
  year      = {2017},
  url       = {http://arxiv.org/abs/1703.00121},
  eprinttype = {arXiv},
  eprint    = {1703.00121},
  timestamp = {Mon, 02 Dec 2019 09:32:19 +0100},
  biburl    = {https://dblp.org/rec/journals/corr/ChengHL17.bib},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44246)  [[Extended]](https://www.openml.org/d/44324)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44290) of an [OpenML dataset](https://www.openml.org/d/44290). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44290/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44290/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44290/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

